import os
import cv2

dataset_folders = ['dataset/1', 'dataset/2', 'dataset/5', 'dataset/10']
dataset_normal_folders = ['dataset_normal/train/1', 'dataset_normal/train/2', 'dataset_normal/train/5',
                          'dataset_normal/train/10']

for i in range(len(dataset_folders)):
    files = os.listdir(dataset_folders[i])
    for j in range(0, len(files), 2):
        image = cv2.imread(dataset_folders[i] + '/' + files[j])
        f = open(dataset_folders[i] + '/' + files[j + 1])
        print(dataset_folders[i] + "/" + files[j])
        x, y, width, height = map(float, f.readline().split(', '))
        if x > 1:
            x /= image.shape[1]
            width /= image.shape[1]
            y /= image.shape[0]
            height /= image.shape[0]
        with open(dataset_normal_folders[i] + '/' + files[j + 1], 'w') as wf:
            wf.write(f"{x} {y} {width} {height}")
        # photo = open(dataset_folders[i] + '/' + files[j], 'rb')
        # with open(dataset_normal_folders[i] + '/' + files[j], 'wb') as wbf:
        #     wbf.write(photo.read())

        cropped = image[int(y * image.shape[0]) : int(y * image.shape[0] + height * image.shape[0]),
                  int(x * image.shape[1]) : int(x * image.shape[1] + width * image.shape[1])]
        cv2.imwrite(dataset_normal_folders[i] + '/' + files[j], cropped)