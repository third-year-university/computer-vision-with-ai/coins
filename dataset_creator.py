import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm


def make_colors(n):
    colors = []
    for i in range(n):
        colors.append(tuple(np.array(cm.tab10(i))[:3] * 255))
    return colors


def read_img():
    global road
    global road_copy
    global segments
    global markers
    global updated
    global file_index
    global file
    global FILE_NAME

    FILE_NAME += 1

    file_index += 1
    if file_index == len(files):
        exit()
    file = files[file_index]

    road = cv2.imread(DIR + "/" + file)

    print(road.shape)
    min_side = min(road.shape[:2])
    max_side = max(road.shape[:2])
    diff = (max_side - min_side) // 2

    if road.shape[0] == min_side:
        road = road[:, diff: road.shape[1] - diff]
    else:
        road = road[diff: road.shape[0] - diff, :]
    print(road.shape)
    road = cv2.resize(road, (500, 500), interpolation=cv2.INTER_AREA)
    road_copy = road.copy()
    segments = np.zeros_like(road)
    markers = np.zeros(road.shape[:-1], dtype="int32")
    updated = False


colors = make_colors(10)

cv2.namedWindow("Image")
cv2.namedWindow("Segments")

current_marker = 1

road = None
road_copy = None
segments = None
markers = None
updated = None
file_index = None
file = None

FILE_NAME = 1
DIR = "coins"
files = os.listdir(DIR)
file_index = -1
file = files[file_index]
read_img()


def mouse_callback(event, x, y, flags, param):
    global updated
    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.circle(markers, (x, y), 10, current_marker, -1)
        cv2.circle(road_copy, (x, y), 10, colors[current_marker], -1)
        updated = True


cv2.setMouseCallback("Image", mouse_callback)

while True:
    cv2.imshow("Image", road_copy)
    cv2.imshow("Segments", segments)
    key = cv2.waitKey(1)
    if key == ord('n'):
        read_img()
    if key == 27:
        break
    elif key > 0 and chr(key).isdigit():
        current_marker = int(chr(key))
    elif key == ord('c'):
        road_copy = road.copy()
        markers[:] = 0
        segments[:] = 0
    elif key == ord('s'):
        data = np.where(np.array(segments) == np.array(colors[current_marker]))
        x_min = min(data[1])
        y_min = min(data[0])
        x_maxim = max(data[1])
        y_maxim = max(data[0])

        my_x = x_min / road.shape[1]
        my_y = y_min / road.shape[0]
        my_height = (y_maxim - y_min) / road.shape[0]
        my_width = (x_maxim - x_min) / road.shape[1]

        if current_marker == 1:
            cv2.imwrite("output/1/DOLBAK_1000" + str(FILE_NAME) + ".jpg", road)
            with open("output/1/DOLBAK_1000" + str(FILE_NAME) + ".txt", "w") as f:
                f.write(f"{my_x}, {my_y}, {my_width}, {my_height}")
        if current_marker == 2:
            cv2.imwrite("output/2/DOLBAK_1000" + str(FILE_NAME) + ".jpg", road)
            with open("output/2/DOLBAK_1000" + str(FILE_NAME) + ".txt", "w") as f:
                f.write(f"{my_x}, {my_y}, {my_width}, {my_height}")
        if current_marker == 3:
            cv2.imwrite("output/5/DOLBAK_1000" + str(FILE_NAME) + ".jpg", road)
            with open("output/5/DOLBAK_1000" + str(FILE_NAME) + ".txt", "w") as f:
                f.write(f"{my_x}, {my_y}, {my_width}, {my_height}")
        if current_marker == 4:
            cv2.imwrite("output/10/DOLBAK_1000" + str(FILE_NAME) + ".jpg", road)
            with open("output/10/DOLBAK_1000" + str(FILE_NAME) + ".txt", "w") as f:
                f.write(f"{my_x}, {my_y}, {my_width}, {my_height}")

    if updated:
        updated = False
        markers_copy = markers.copy()
        cv2.watershed(road, markers_copy)
        segments = np.zeros(road.shape, dtype="uint8")
        for i in range(10):
            segments[markers_copy == i] = colors[i]

cv2.destroyAllWindows()