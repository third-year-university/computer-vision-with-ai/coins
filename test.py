import os

import cv2
import keras
import numpy as np
from matplotlib import pyplot as plt

input_shape = (150, 150, 3)

model = keras.models.load_model('coins2.h5')

total = 0
good = 0

dataset_normal_folders = ['dataset_normal/test/1', 'dataset_normal/test/2', 'dataset_normal/test/5',
                          'dataset_normal/test/10']

for i in range(len(dataset_normal_folders)):
    files = os.listdir(dataset_normal_folders[i])
    for j in range(0, len(files)):
        img = plt.imread(dataset_normal_folders[i] + '/' + files[j])
        img = cv2.resize(img, (input_shape[:2]))
        img = img[np.newaxis, ...]
        print(model.predict_classes(img / 255.))