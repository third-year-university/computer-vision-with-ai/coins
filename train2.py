import keras
from keras import Sequential
from keras.preprocessing.image import ImageDataGenerator

input_shape = (150, 150, 3)

gen = ImageDataGenerator(rotation_range=30, width_shift_range=0.1, height_shift_range=0.1,
                          rescale=1./255, shear_range=0.2, zoom_range=0.2,horizontal_flip=True,
                            fill_mode="nearest")

batch_size=16

train_gen = gen.flow_from_directory("dataset_normal/train", target_size=input_shape[:2],
                                    batch_size=batch_size, class_mode="binary")

train_gen.class_indices

test_gen = gen.flow_from_directory("dataset_normal/test", target_size=input_shape[:2],
                                    batch_size=batch_size, class_mode="binary")

test_gen.class_indices
test_gen.next()

batch, classes = test_gen.next()
# plt.show()

model = keras.models.load_model('coins.h5')

modeltrain_result = model.fit(train_gen, epochs=20, steps_per_epoch=10,
                              validation_data=test_gen, validation_steps=10)

model.save("coins2.h5")